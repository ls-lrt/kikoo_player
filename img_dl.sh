#!/bin/bash

search=$(echo ${1:-kikoo} | tr ' _\-\(\)' '+')
max_dl=2
img_size="xga" #1024x768
url="https://www.google.com/search?&tbm=isch&nfpr=1&q=${search}&hl=en&site=imghp&ijn=0&start=0&tbs=isz:lt,islt:${img_size}"

wget "$url" --quiet --user-agent 'kikoo' -O - | grep -oP "imgurl=\K[^&]*?\.((png)|(jpe?g))" | head -n $max_dl | while read img_url; do
    wget --quiet --user-agent 'kikoo' "$img_url"
done
