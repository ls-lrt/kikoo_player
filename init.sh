#!/bin/bash

dir=${1:-..}
script_abs_path=$(dirname "$(readlink -f ${0})")"/"
index_html="${script_abs_path}index.html"
player_js="${script_abs_path}player.js"
player_dir=$(basename "${script_abs_path}")
img_dl="${script_abs_path}img_dl.sh"
player_html=""

if [ ! -f "$index_html" ]; then
    echo "$index_html not found, generating it..."
    echo "Please give the url where the player will be located, eg:
http://example.com/music/kikoo_player or leave it empty to put it in
index.html file."
    read -p "player location: " player_location
    if [ "X"$player_location = "X" ]; then
	player_html="<script>"$(cat $player_js)"</script>"
    else
	player_html="<script type=\"text/javascript\" src=\"${player_location}\"></script>"
    fi
    cat <<EOF > $index_html
<html>
  <title>.:Kikoo player:.</title>
  <meta charset="UTF-8">
  $player_html
  <body background="cover" style="background-position: center; background-repeat: no-repeat; background-attachment: fixed;">
    <div id="playlist"></div>
    <div id="player" style="position:absolute;bottom:0;background-color:#ffffff;width:99%;"></div>
  </body>
</html>
EOF
fi

get_cover()
{
    for i in *.[pPjJ][nNpP][gG]; do
	if [ ! "$i" = "*.[pPjJ][nNpP][gG]" ]; then
	    echo "$i" #TODO use the larger image if more than one is found
	    break
	fi
    done
}

walk()
{
    cd "$1"
    local dirname=$(basename "$(pwd)")
    if [ "$dirname" != "$player_dir" ]; then
	if [ $(find -type d -not -path '*/\.*' | wc -l) -eq 1 ]; then
	    find -name "*.mp3" | sort > "playlist.m3u" && rm -f index.html && ln -s "$index_html" .
	    local album="$dirname"
	    local artist=$(basename "$(dirname "$(pwd)")")
	    local cover=$(get_cover)

	    if [ "X$cover" = "X" ]; then
		$img_dl "$artist $album"
		cover=$(get_cover)
	    fi
	    if [ "X$cover" != "X" ]; then
		rm -f cover
		ln -s "$cover" cover
	    fi
	else
	    for i in *; do
	    if [ -d "$i" ]; then
		echo -n "."
		walk "$i"
	    fi
	    done
	fi
    fi
    cd ..
}

echo -n "Initializing..."
walk "$dir"
echo "done!"
