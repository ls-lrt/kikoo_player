#!/bin/bash

from=${1:-wma}
to=${2:-mp3}

for i in *.${from}; do
    ffmpeg -nostdin -i "$i" -map_metadata 0:s:0 "${i%$from}$to"
done
