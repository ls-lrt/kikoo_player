# kikoo_player

Dummy player to read mp3 (based on web browser capabilities) files
served by an http server.

# Example of use

Immagine you have mp3 files stored on a private location
/private/music and that directory is served by an http server
(like apache fr instance). All you have to do is to clone this project
on top of your music directory then run init.sh script (bash needed
since it contains some "bashisms").

```
 user@host:~/private/music $ git clone https://github.com/herbert-de-vaucanson/kikoo_player.git
 user@host:~/private/music $ cd kikoo_player
 user@host:~/private/music/kikoo_player $ ./init.sh ../
 kikoo_player/index.html not found, generating it...
  Please give the url where the player will be located, eg:
  http://example.com/music/kikoo_player or leave it empty to put it in
  index.html file.
  player location: /private/private/music/kikoo_player/player.js
  Initializing.........................done!

```

# Under the hood

The script walk recursively to "$1" (../ if nothing specified) and
produce a file playlist.m3u conatining a list of all .mp3 found in the
directory. It also make a symlink to index.html that will be loaded by
default by mostly all http server, that will start the player.

The script will also look for some pictures (png, jpg) and make a
symlink "cover" on the first one that is found.
If no image is found, the script tries to download two images based on
the name of the parent directory (assumed to the the artist name) and
the current one (assumed to be the album name).
