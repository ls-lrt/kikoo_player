var xhr = new XMLHttpRequest();
xhr.onreadystatechange = process;
xhr.open("GET", "playlist.m3u", true);
xhr.send();

// hum hugly hugly hugly
var tracks = [];
var current = -1;
var song;
var songs = [];
var preload = false;
var player;
var is_playing = 1;
var retry_timeout = 1000;
var retry_action = undefined;
var delta = 5; //seek
var pause_html = " &#9646;&#9646 ";
var play_html = " &#9654 ";
var seek_forward_html = " &#9193 ";
var seek_backward_html = " &#9194 ";

Audio.prototype.time_html = function() {
    var sec = Math.floor(this.currentTime);
    var min = Math.floor(sec / 60);
    var len_s = Math.floor(this.duration);
    var len_min = Math.floor(len_s / 60);
    return (min ? min + "'": "" ) + sec % 60 + "\" / " + (len_min ? len_min + "'": "" ) + len_s % 60 + "\"";
}

String.prototype.name_html = function() {
    return this.substr(this.lastIndexOf('/') + 1, this.lastIndexOf('.') - 2).replace(/_/g, " ");
}

function process()
{
    if (xhr.readyState === XMLHttpRequest.DONE) {
	var playlist = xhr.responseText;
	tracks = playlist.match(/[^\r\n]+/g);
	display_playlist();
	start_playing();
    }
}

function display_playlist(t) {
    var playlist_txt = document.getElementById("playlist");
    for (t in tracks) {
	playlist_txt.innerHTML += '<a href="' + tracks[t] +'"> o </a> <span id="' + t + '">' + tracks[t].name_html() + '</span></br>';
    }
}

function play(cb_fail) {
    if (retry_action)
	clearTimeout(retry_action);

    if (current < 0)
	return retry_action = setTimeout(cb_fail, retry_timeout);

    if (typeof song === 'undefined') {
	if (preload) {
	    if (typeof songs[current].cache !== 'undefined')
		song = new Audio(songs[current].cache);
	    else
		return retry_action = setTimeout(cb_fail, retry_timeout);
	} else {
	    song = new Audio(tracks[current]);
	}
	song.addEventListener("ended", next);
	song.addEventListener("canplay", song.play);
    }

    if (current < tracks.length) {
	player.innerHTML = play_html + tracks[current].name_html();
	if (preload) {
	    if (typeof songs[current].cache !== 'undefined')
		song.src = songs[current].cache;
	    else
		return retry_action = setTimeout(cb_fail, retry_timeout);
	} else {
	    song.src = tracks[current];
	}
    } else {
	var again = confirm("Replay?");
	if (again)
	    restart();
    }
}

function pause() {
    if (typeof song !== 'undefined') {
	if (is_playing) {
	    song.pause()
	    player.innerHTML = pause_html + tracks[current].name_html() + " " + song.time_html();
	} else {
	    song.play();
	    player.innerHTML = play_html + tracks[current].name_html();
	}
	is_playing = !is_playing;
    }
}

function next() {
    current++;
    play(next);
}

function prev() {
    current--;
    play(prev);
}

function restart() {
    current = 0;
    play(restart);
}

function seek_at(t, seek_symbol) {
    if (typeof song !== 'undefined') {
	song.pause();
	song.currentTime = t;
	player.innerHTML = seek_symbol + tracks[current].name_html() + " " + song.time_html();
	song.play();
	setTimeout(function()
		   {
		       player.innerHTML = play_html + tracks[current].name_html();
		   }, 500);
    }
}

function seek_up() {
    seek_at(song.currentTime + delta, seek_forward_html);
}

function seek_down() {
    seek_at((song.currentTime < delta) ? 0 : song.currentTime - delta, seek_backward_html);
}

function my_song(i) {
    var that = this;

    this.html = document.getElementById(i);
    this.timer = undefined;
    this.req = new XMLHttpRequest();
    this.req.open("GET", tracks[i], true);
    this.req.responseType = "blob";
    this.base = this.html.innerHTML;

    this.req.onload = function() {
	that.html.innerHTML = that.base;
	that.cache = URL.createObjectURL(that.req.response);
	if (current < 0) {
	    current = i;
	    play(play);
	}
	clearInterval(that.timer);
    };

    this.req.onreadystatechange = function(ev) {
	if (this.readyState === XMLHttpRequest.LOADING) {
	    var j = 0;
	    var txt = that.html.innerHTML.split("");
	    if (typeof that.timer === 'undefined')
		that.timer = setInterval(function() {
		    that.html.innerHTML = txt.slice(0,j++).join("");
		    j %= txt.length + 1;
		}, 200);
	}
    };
    this.req.send();
};

function start_playing()
{
    preload = confirm("Activating preload mode?");
    player = document.getElementById("player");
    if (preload) {
	for (var i = 0; i < tracks.length; i++)
	    songs[i] = new my_song(i);
    }
    if (!preload) {
	current = 0;
	play(play);
    }
}

document.addEventListener('keydown', function(ev) {
    var to;
    switch (ev.keyCode) {
    case 32: // space
    case 80: // p
	pause();
	break;
    case 37: // <-
	prev();
	break;
    case 39: // ->
	next();
	break;
    case 38: // arrow_up
	seek_up();
	break;
    case 40: // arrow_down
	seek_down();
	break;
    default:
	console.log(ev.keyCode);
    }
});

// Handling touch events
var timer;
var touch_t = 50;
var x = null;
var y = null;

document.addEventListener('touchstart', function(ev) {
    x = ev.touches[0].clientX;
    y = ev.touches[0].clientY;
    // pause if press for touch_t ms
    timer = setTimeout(pause, touch_t);
}, false);

document.addEventListener('touchend', function() {
    if (timer)
        clearTimeout(timer);
}, false);

document.addEventListener('touchmove', function(ev) {
    if (timer)
        clearTimeout(timer);

    if ( !x || !y )
        return;

    var delta_x = x - ev.touches[0].clientX;
    var delta_y = y - ev.touches[0].clientY;;

    if (Math.abs(delta_x) > Math.abs(delta_y)) {
        if (delta_x > 0)
	    prev();
	else
	    next();
    } else {
        if (delta_y > 0)
	    seek_up();
	else
	    seek_down();
    }

    x = null;
    y = null;
}, false);
